export const getCountryName = (lang) => {
  switch (lang) {
    case "en":
      return "English";
    case "vi":
      return "Vietnamese";
    case "ja":
      return "Japanese";
    case "ko":
      return "Korea";
    case "pt":
      return "Portuguese";
    case "es":
      return "Spanish";
    case "ar":
      return "Arabic";
    default:
      return "English";
  }
};

// https://snapapps.sfo3.digitaloceanspaces.com/mushroom/post/1693989134810_1693988898672_d0ab47b5-edd0-41fc-a255-f5601fe41e87.jpg

export const getAppName = (appName) => {
  switch (appName) {
    case "coinid":
      return "coin";
    case "coin":
      return "coin";
    case "insectid":
      return "insect";
    case "insect":
      return "insect";
    case "plantid":
      return "plant";
    case "plant":
      return "plant";
    case "mushroomai":
      return "mushroom";
    case "mushroom":
      return "mushroom";
    case "rockspot":
      return "rockspot";
    case "stone":
      return "stone";
    case "fishdetect":
      return "fish";
    case "fish":
      return "fish";
    case "birddetect":
      return "bird";
    case "bird":
      return "bird";
  }
};
