import React, { useEffect } from 'react';
import { Box, Modal, Typography } from '@mui/material';
import Image from 'next/image';
import WrapQRcode from './WrapQRcode';
import CloseIcon from '@mui/icons-material/Close';
import PropTypes from 'prop-types';
import { useTranslation } from 'next-i18next';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  width: { xs: '90%', lg: '900px' },
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: 'none',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '12px',
  padding: { xs: '20px 10px', md: '40px 60px 60px 60px' },
};

const ModalDownload = ({ open, handleClose, itemForDownload }) => {
  const { t: translate } = useTranslation('common');
  const [app, setApp] = React.useState({
    name: '',
    logoPath: '',
    link: '',
    qrCode: '',
    linkAndroid: '',
    linkIos: '',
  });

  useEffect(() => {
    if (itemForDownload) {
      setApp(itemForDownload);
    }
  }, [itemForDownload]);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      sx={{
        '& > div': {
          '&:first-of-type': {
            backgroundColor: 'rgba(0,0,0,0.2)',
          },
        },
      }}
    >
      <Box sx={style}>
        <Image src={app?.logoPath} width={148} height={148} alt="mushroom" />
        <Typography
          variant="h3"
          sx={{
            fontWeight: '600',
            fontSize: { xs: '20px', md: '24px', lg: '36px' },
            padding: '10px 0',
            color: '#333333',
            textAlign: 'center',
          }}
        >
          {translate(`app.${app?.link}.title`)}
        </Typography>
        <Box
          sx={{
            width: { xs: '100%', md: '530px' },
            paddingBottom: '40px',
            '& h6': {
              fontWeight: '400',
              fontSize: { xs: '16px', md: '18px' },
              textAlign: 'center',
              color: '#333',
              paddingTop: '16px',
            },
          }}
        >
          <Typography variant="h6">
            {translate(`app.${app?.link}.text01`)}
          </Typography>
          <Typography variant="h6">
            {translate(`app.${app?.link}.text02`)}
          </Typography>
        </Box>
        <WrapQRcode
          appName={app?.name}
          linkIos={app?.linkIos}
          linkAndroid={app?.linkAndroid}
        />
        <Typography
          sx={{
            color: '#333333',
            fontSize: { xs: '16px', md: '18px' },
          }}
        >
          {translate(`app.${app?.link}.scan`)}
        </Typography>
        <CloseIcon
          sx={{
            position: 'absolute',
            top: '20px',
            right: '20px',
            cursor: 'pointer',
            color: '#F56310',
            '&:hover': {
              color: '#F56310',
              opacity: '0.6',
            },
          }}
          onClick={handleClose}
        />
      </Box>
    </Modal>
  );
};

ModalDownload.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  itemForDownload: PropTypes.object,
};

export default ModalDownload;
