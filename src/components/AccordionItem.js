/* eslint-disable react/prop-types */
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import React from 'react';
import { useTranslation } from 'next-i18next';

const AccordionItem = ({ number, content }) => {
  const { t: translate } = useTranslation('common');

  return (
    <Accordion
      sx={{
        padding: { xs: '0', md: '12px 40px' },
        margin: '10px 0',
        border: '1px solid #f22447',
        borderRadius: '12px !important',
        boxShadow: '0px 2px 15px rgba(0,0,0,0.1)',
        '& .MuiCollapse-root': {
          '& .MuiCollapse-wrapper': {
            paddingTop: '20px',
            borderTop: '1px solid #E1E6E5',
            fontSize: '18px',
            '&  .MuiTypography-root': {
              fontSize: '18px',
              color: '#333',
            },
          },
        },
        '& .MuiTypography-root': {
          fontSize: { xs: '20px', md: '24px' },
          fontWeight: '400',
          color: '#000',
        },
      }}
    >
      <AccordionSummary
        expandIcon={
          <ExpandMoreIcon
            sx={{
              fontSize: { xs: '30px', md: '40px' },
            }}
          />
        }
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography>{translate(`faqs.${number}.title`)}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        {content?.map((item, index) => (
          <Typography key={index}>
            {translate(`faqs.${number}.description`)}
          </Typography>
        ))}
      </AccordionDetails>
    </Accordion>
  );
};

export default AccordionItem;
