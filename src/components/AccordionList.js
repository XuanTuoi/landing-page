/* eslint-disable react/prop-types */
import { Box, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'next-i18next';
import AccordionItem from './AccordionItem';

const AccordionList = ({ i, listAccordions }) => {
  const { t: translate } = useTranslation('common');
  console.log('translate', translate('title1'));
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        margin: { xs: '0 ', md: '16px auto' },
        width: { xs: '100%', lg: '960px' },
      }}
    >
      <Typography
        variant="h2"
        sx={{
          padding: { xs: '32px 0 18px 0', md: '40px 0 26px 0' },
          fontSize: { xs: '21px', md: '30px' },
          fontWeight: { xs: '600', md: '400' },
        }}
      >
        {i === '0' ? translate('faqs.title1') : translate('faqs.title2')}
      </Typography>
      <Box>
        {listAccordions.map((item, index) => {
          return (
            <AccordionItem
              key={index}
              number={item?.key}
              title={item?.title}
              content={item?.description}
            />
          );
        })}
      </Box>
    </Box>
  );
};

export default AccordionList;
