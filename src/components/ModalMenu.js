/* eslint-disable react/prop-types */
import React from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Modal,
  Slide,
  Typography,
} from '@mui/material';

import CloseIcon from '@mui/icons-material/Close';
import { countryName, listAnimal, listApp } from '../contants';
import Link from 'next/link';
import { Roboto } from 'next/font/google';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import ModalDownload from './Modal';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { getCountryName } from '../ultils';
import { LanguageSwitcher } from './LanguageSwicher';
const RobotoFont = Roboto({
  subsets: ['latin'],
  weight: ['400', '500', '700'],
});

const style = {
  position: 'absolute',
  width: '100%',
  height: '100%',
  overflow: 'auto',
  bgcolor: 'background.paper',
  p: 2,
  '&  .MuiPaper-root': {
    boxShadow: 'none',
    '&::before': {
      display: 'none',
    },
    '& .MuiButtonBase-root': {
      padding: '0',
    },
    '& .MuiAccordionSummary-content': {
      margin: '6px 0',
    },
  },
  '&::-webkit-scrollbar': {
    display: 'none',
  },
};

const ModalMenu = ({
  itemForDownload,
  open,
  setOpen,
  handleOpen,
  openMenu,
  setOpenMenu,
  appLanguage,
  setAppLanguage,
}) => {
  const { locale, locales, push } = useRouter();
  const { t: translate } = useTranslation('common');
  const [showApp, setShowApp] = React.useState(false);
  const [showPageAnimal, setShowPageAnimal] = React.useState(false);
  const [openDownload, setOpenDownload] = React.useState(false);
  const handleClose = () => {
    setOpenMenu(false);
    setShowPageAnimal(false);
    setShowApp(false);
    setOpenDownload(false);
  };
  return (
    <Modal
      open={openMenu}
      onClose={handleClose}
      className={RobotoFont.className}
    >
      <Slide direction="right" in={openMenu} mountOnEnter unmountOnExit>
        <Box sx={style}>
          <CloseIcon
            sx={{
              position: 'absolute',
              right: '24px',
              cursor: 'pointer',
              fontSize: '24px',
              color: '#333',
            }}
            onClick={handleClose}
          />
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              fontSize: '24px',
              marginLeft: 'auto',
              height: '100%',
              mt: '24px',
              '& a': {
                padding: '12px 0',
                cursor: 'pointer',
                transition: 'all 0.2s ease',
              },
              '& > a,p': {
                width: '100%',
                color: '#333',
              },
            }}
          >
            <Link
              href="/"
              onClick={handleClose}
              style={{
                display: 'block',
                cursor: 'pointer',
                fontSize: '18px',
                fontWeight: '600',
              }}
            >
              {translate('home')}
            </Link>
            <Box
              sx={{
                position: 'relative',
                width: '100%',
              }}
            >
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography
                    sx={{
                      padding: '12px 0',
                      fontSize: '18px',
                      cursor: 'pointer',
                      userSelect: 'none',
                      fontWeight: '600',
                      textAlign: 'left',
                    }}
                    onClick={() => setOpenDownload(!openDownload)}
                  >
                    {translate('download')}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  {listApp.map((item, index) => {
                    return (
                      <Box
                        key={index}
                        sx={{
                          width: '100%',
                        }}
                      >
                        <Typography
                          sx={{
                            padding: '16px 0',
                            fontWeight: '400',
                            textAlign: 'left',
                            color: '#333',
                            fontSize: '16px',
                            cursor: 'pointer',
                            transition: 'all 0.2s ease',
                            border: 'none !important',
                          }}
                          onClick={() => handleOpen(item)}
                        >
                          {item.name}
                        </Typography>
                        <ModalDownload
                          open={open}
                          handleClose={() => {
                            setOpen(false);
                            handleClose();
                          }}
                          itemForDownload={itemForDownload}
                        />
                      </Box>
                    );
                  })}
                </AccordionDetails>
              </Accordion>
            </Box>
            <Box
              sx={{
                width: '100%',
                position: 'relative',
              }}
              onClick={() => setShowPageAnimal(!showPageAnimal)}
            >
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography
                    sx={{
                      padding: '12px 0',
                      cursor: 'pointer',
                      userSelect: 'none',
                      fontWeight: '600',
                      fontSize: '18px',
                    }}
                  >
                    {translate('animal')}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  {listAnimal.map((item, index) => {
                    return (
                      <Link
                        href={`/animal/${item.link}` || ''}
                        key={index}
                        onClick={handleClose}
                        style={{
                          display: 'block',
                          padding: '16px 0',
                          fontWeight: '400',
                          color: '#333',
                          fontSize: '16px',
                          cursor: 'pointer',
                          transition: 'all 0.2s ease',
                          textAlign: 'left',
                          border: 'none !important',
                        }}
                      >
                        {translate(item.name)}
                      </Link>
                    );
                  })}
                </AccordionDetails>
              </Accordion>
            </Box>
            <Box
              sx={{
                width: '100%',
                position: 'relative',
              }}
              onClick={() => setShowApp(!showApp)}
            >
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography
                    sx={{
                      padding: '12px 0',
                      fontSize: '18px',
                      cursor: 'pointer',
                      userSelect: 'none',
                      fontWeight: '600',
                    }}
                  >
                    {translate('application')}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  {listApp.map((item, index) => {
                    return (
                      <Link
                        href={`/application/${item.link}` || ''}
                        key={index}
                        onClick={handleClose}
                        style={{
                          display: 'block',
                          padding: '16px 0',
                          fontWeight: '400',
                          color: '#333',
                          fontSize: '16px',
                          cursor: 'pointer',
                          transition: 'all 0.2s ease',
                          textAlign: 'left',
                          border: 'none !important',
                        }}
                      >
                        {item.name}
                      </Link>
                    );
                  })}
                </AccordionDetails>
              </Accordion>
            </Box>
          {/* <Link href="/faq" onClick={handleClose}>
            FAQ
          </Link> */}
            <LanguageSwitcher locale={locale}></LanguageSwitcher>
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography
                  sx={{
                    padding: '12px 0',
                    fontSize: '18px',
                    cursor: 'pointer',
                    userSelect: 'none',
                    fontWeight: '600',
                  }}
                  
                >
                  {countryName(appLanguage)}
                </Typography>
              </AccordionSummary> 
              <AccordionDetails>
                {locales.map((item, index) => {
                  return (
                    <Typography
                      value={item}
                      key={index}
                      onClick={() => setAppLanguage(item)}
                      sx={{
                        padding: '16px 0',
                        fontWeight: '400',
                        textAlign: 'left',
                        color: '#333',
                        fontSize: '16px',
                        cursor: 'pointer',
                        transition: 'all 0.2s ease',
                        border: 'none !important',
                      }}
                    >
                      {countryName(item)}
                    </Typography>
                  );
                })}
              </AccordionDetails>
            </Accordion>
          </Box>
        </Box>
      </Slide>
    </Modal>
  );
};

export default ModalMenu;
