/* eslint-disable react/prop-types */
import { Box } from "@mui/material";
import Link from "next/link";
import React from "react";

const WrapImg = ({ link, linkIos, linkAndroid, logoPath }) => {
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: { xs: "center", md: "space-between" },
        margin: { sm: "10px 20px", md: "10px 60px" },
        width: { xs: "100%", md: "auto" },
      }}
    >
      <Box
        sx={{
          width: "120px",
          height: "120px",
          position: "relative",
          "&  img": {
            objectFit: "cover",
            borderRadius: "10px",
          },
        }}
      >
        <Link href={`/application/${link}` || ""}>
          <img
            src={logoPath}
            alt={`app's logo`}
            style={{ objectFit: "fill", width: "120px", height: "120px" }}
          />
        </Link>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          marginLeft: "40px",
          "& > a": {
            margin: "10px 0",
          },
        }}
      >
        <Link href={linkIos || "/"} target='_blank'>
          <img
            alt='ios'
            src='/ios.webp'
            width={156}
            height={52}
            style={{ objectFit: "fill" }}
          />
        </Link>
        <Link href={linkAndroid || "/"} target='_blank'>
          <img
            alt='android'
            src='/android.webp'
            width={156}
            height={52}
            style={{ objectFit: "fill" }}
          />
        </Link>
      </Box>
    </Box>
  );
};

export default WrapImg;
