/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import QRCode from "qrcode.react";

const WrapQRcode = ({ appName, linkIos, linkAndroid, style }) => {
  const [value, setValue] = useState("");
  useEffect(() => {
    if (appName) {
      switch (appName) {
        case "Coin ID":
          setValue("https://direct-link.snapapps.online/app/coin-ai");
          break;
        case "Insect ID":
          setValue("https://direct-link.snapapps.online/app/insect-ai");
          break;
        case "PlantID":
          setValue("https://direct-link.snapapps.online/app/find-plant");
          break;
        case "MushroomAI":
          setValue("https://direct-link.snapapps.online/app/mushroom");
          break;
        case "RockSpot":
          setValue("https://direct-link.snapapps.online/app/stone");
          break;
        case "FishDetect":
          setValue("https://direct-link.snapapps.online/app/fish");
          break;
        case "BirdDetect":
          setValue("https://direct-link.snapapps.online/app/bird");
          break;
      }
    }
  }, [appName]);

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: { xs: "center", md: "space-between" },
        margin: { sm: "10px 20px", md: "10px 60px" },
        width: { xs: "100%", md: "auto" },
        ...style,
      }}
    >
      <QRCode value={value} />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          marginLeft: "40px",
          "& > a": {
            margin: "10px 0",
          },
        }}
      >
        <Link href={linkIos || "/"} target='_blank'>
          <Image
            alt='ios'
            src='/ios.webp'
            width={156}
            height={52}
            style={{ objectFit: "fill" }}
          />
        </Link>
        <Link href={linkAndroid || "/"} target='_blank'>
          <Image
            alt='android'
            src='/android.webp'
            width={156}
            height={52}
            style={{ objectFit: "fill" }}
          />
        </Link>
      </Box>
    </Box>
  );
};

export default WrapQRcode;
