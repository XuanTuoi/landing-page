import { Box, IconButton } from "@mui/material";
import { useEffect, useState } from "react";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
export default function ScrollToTop() {
    const [showTopBtn, setShowTopBtn] = useState(false);
    useEffect(() => {
        window.addEventListener('scroll', () => {
            if (window.scrollY > 400) {
                setShowTopBtn(true);
            } else {
                setShowTopBtn(false);
            }
        });
    }, []);
      const gotoTop = () => {
        window.scrollTo({
          top: 0,
          behavior: "smooth",
        });
      };
    return (
        <>
            {showTopBtn && (
            <Box
              sx={{
                width: "40px",
                height: "40px",
                borderRadius: "50%",
                backgroundColor: "#F56310",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                position: {xs:'static',sm:'fixed'},
                bottom: "24px",
                zIndex:'10',
                right: "28px",
              }}
            >
              <IconButton onClick={gotoTop}>
                <KeyboardArrowUpIcon sx={{ color: "#fff" }} />
              </IconButton>
            </Box>
          )}
        </>
    )
}