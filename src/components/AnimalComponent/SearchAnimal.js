import { Search } from "@mui/icons-material";
import { Box, IconButton, TextField } from "@mui/material";

export default function SearchByAnimal(props) {
    return (
        <Box
        sx={{
          marginBottom: "15px",
          textAlign: "right",
        }}
      >
        <form
          onSubmit={props.handleSearchSubmit}
          style={{ position: "relative" }}
        >
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            type="search"
            name="searchKey"
            inputProps={{
              sx: { padding: "9.5px 33px 9.5px 9.5px" },
            }}
            defaultValue={props.search}
            sx={{ position: "relative", top: 0, left: 0, right: 0 }}
          />
          <IconButton
            type="submit"
            sx={{ position: "absolute", right: "0px", top: "1px" }}
          >
            <Search />
          </IconButton>
        </form>
      </Box>
    )
}