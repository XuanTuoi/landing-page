"use client";
import {
  Box,
  Typography,
  Card,
  CardContent,
  Stack,
  Pagination,
  Container,
  Grid,
} from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import ScrollToTop from "./ScrollToTop";
import SearchByAnimal from "./SearchAnimal";

export default function PagesAnimal({
  data,
  search,
  name,
  currentPage,
  setCurrentPage,
}) {
  const router = useRouter();
  const pathname = usePathname();
  const handlePageChange = (e, value) => {
    setCurrentPage(value);
    router.push(`${name}?page=${value}${search ? `&searchQ=${search}` : ""}`, {
      scroll: true,
    });
  };
  const handleSearchSubmit = (event) => {
    event.preventDefault();
    router.push(`${name}?searchQ=${event.target.searchKey.value}`);
  };
  return (
    <Box>
      <ScrollToTop />
      {data?.datas.length === 0 ? (
        <Box
          sx={{
            backgroundColor: "#fff",
            minHeight: "800px",
            padding: "0px 12px",
            color: "#000",
          }}
        >
          <Container sx={{ padding: "12px 0px" }}>
            <SearchByAnimal search={search} name={name} />
            <Typography sx={{ lineHeight: "55px", textAlign: "center" }}>
              Sorry, no animals were found for your selection
            </Typography>
          </Container>
        </Box>
      ) : (
        <Container sx={{ padding: "12px 0px" }}>
          {data && (
            <SearchByAnimal
              search={search}
              name={name}
              handleSearchSubmit={handleSearchSubmit}
            />
          )}
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            {data?.datas.map((item, index) => {
              return (
                <Grid key={index} item xs={4} sm={4} md={4}>
                  <Card
                    sx={{
                      width: "auto",
                      position: "relative",
                      minHeight: { md: "300px", xs: '250px', lg: "300px"},
                      boxShadow: "0 0 15px -2px rgba(0,0,0,.75)",
                      border: "1px solid rgba(0,0,0,.125)",
                    }}
                    key={index}
                  >
                    {/* ?.split(" ").join("-") */}
                    <Link
                      href={{
                        pathname: `${pathname}/${item.classification.scientificName?.replaceAll(/\s/g,"-")}`,
                      }}
                    >
                      <Image
                        src={
                          item.images.length == 0
                            ? "/default.png"
                            : item.images[0]
                        }
                        style={{ objectFit: "cover" }}
                        alt=""
                        fill
                        placeholder="blur"
                        blurDataURL="/default.png"
                        sizes="(min-width: 60em) 24vw,
                            (min-width: 28em) 45vw,
                            100vw"
                      />
                    </Link>
                    <CardContent sx={{ padding: "0 !important" }}>
                      <Box
                        sx={{
                          backgroundColor: "rgba(0,0,0,0.6)",
                          color: "white",
                          height: "90px",
                          position: "absolute",
                          padding: 0,
                          bottom: 0,
                          right: 0,
                          left: 0,
                          padding: "6px 10px",
                        }}
                      >
                        <Typography
                          gutterBottom
                          variant="h5"
                          component="div"
                          sx={{
                            minHeight: "29px",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            wordWrap: "no-wrap",
                            whiteSpace: "nowrap",
                            "& > a:hover": {
                              color: "#F56310",
                              textDecoration: "underline",
                            },
                          }}
                        >
                          <Link
                            href={{
                              pathname: `${pathname}/${item.classification.scientificName.replaceAll(/\s/g,"-")}`,
                            }}
                          >
                            {item.classification.scientificName
                              ?.split(/,|&/g)
                              .shift() ?? item.facts.commonName}
                          </Link>
                        </Typography>
                        <Typography
                          variant="body2"
                          sx={{
                            lineHeight: "1.2em",
                            maxHeight: "4em",
                            display: "-webkit-box",
                            "-webkit-box-orient": "vertical",
                            "-webkit-line-clamp": "2",
                            overflow: "hidden",
                          }}
                          className="translate"
                        >
                          {item.facts.funFact ?? item.facts.prey}
                        </Typography>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
              );
            })}
          </Grid>
          {data && (
            <Stack
              spacing={4}
              sx={{ maxWidth: "max-content", margin: "0 auto" }}
            >
              <Pagination
                count={data?.meta.pageCount}
                onChange={handlePageChange}
                page={currentPage}
                color="primary"
                sx={{
                  marginTop: "15px",
                  "& .Mui-selected": { backgroundColor: "#F56310 !important" },
                }}
              />
            </Stack>
          )}
        </Container>
      )}
    </Box>
  );
}
