import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { useEffect } from "react";
const COOKIE_NAME = "googtrans"
export default function ReloadAnimal() {
    const router = useRouter();
  useEffect(() => {
    let language = router.locale;
    if (language == 'vn') {
        language = 'vi'
    }
    console.log('locale ReloadAnimal', language);
    Cookies.set(COOKIE_NAME, "/auto/" + language);
  }, [router]);
}