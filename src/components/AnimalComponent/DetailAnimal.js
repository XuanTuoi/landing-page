import ReactHtmlParser from "react-html-parser";
import ScrollToTop from "./ScrollToTop";
import Slide from "./Swiper";
import Link from "next/link";
import Image from "next/image";
import {
  Box,
  List,
  ListItem,
  ListItemText,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
} from "@mui/material";
import { useTheme } from '@mui/material/styles';
import { styled } from '@mui/system';

const ResponsiveTableCell = styled(TableCell)(({ theme }) => ({
  [theme.breakpoints.down('sm')]: {
    display: 'block',
    width: '80vw'
  },
}));

export default function DetailAnimalComponent(props) {
  const theme = useTheme();
  return (
    <Box
      sx={{
        backgroundColor: "#fff",
        color: "#000",
      }}
    >
      <Box
        sx={{
          maxWidth: "1200px",
          margin: "0 auto",
          paddingBottom: "40px",
          padding: { xs: "0px 20px ", md: "0px 20px" },
        }}
      >
        <Box
          className="notranslate"
          sx={{
            position: "relative",
            "& .swiper-wrapper .swiper-slide": {
              minHeight: { md: "580px", xs: "300px", sm: "400px" },
            },
            borderRadius: "0",
            boxShadow: "rgba(0, 0, 0, 0.35) 0px -50px 36px -28px inset",
          }}
        >
          <Slide images={props.images}></Slide>


          <Box
            sx={{
              position: "absolute",
              zIndex: "100",
              top: "30%",
              color: "#fff",
              width: "100%",
              padding: "0px 15px",
              left: "50%",
              fontWeight: "900",
              textAlign: "center",
              transform: "translate(-50%, -50%)",
              "& > h1": {
                fontSize: {
                  sm: "2.5rem",
                  md: "4.5rem",
                },
                fontWeight: {
                  md: "900",
                  sm: "800",
                },
                marginBottom: ".5rem",
              },
              "& > p": {
                textAlign: "center",
                fontSize: {
                  lg: "1.4rem",
                  sm: "1.2rem",
                  md: "1.3rem",
                },
                fontWeight: "bolder !important",
                lineHeight: "2em",
              },
            }}
          >
            <h1>
              {props.facts?.commonName
                ? props.facts?.commonName.split(",").shift()
                : props.classification?.scientificName.split(",").shift()}
            </h1>
            <p>
              {props.facts?.commonName
                ? props.classification?.scientificName.split(",").shift()
                : ""}
            </p>

          </Box>
        </Box>
        <ScrollToTop />
        <Grid
          container
          spacing={2}
          columns={{ xs: 4, sm: 8, md: 12 }}
          justifyContent="space-around"
          sx={{
            textAlign: "center",
            width: "auto",
            marginLeft: "0",
            backgroundImage: "linear-gradient(to right,#eee,#eee)",
            "& td": {
              fontSize: "1rem !important",
              letterSpacing: "normal",
            },
            "& .css-iltpxr-MuiGrid-root": {
              flexBasis: { sm: "100%", md: "50%", lg: "50%" },
              maxWidth: { sm: "100%", md: "50%", lg: "50%" },
            },
          }}
        >
          <Grid item xs={6} >
            <Box>
              <Typography
                sx={{ mt: 4, mb: 2, fontWeight: 600, marginBottom: 0 }}
                variant="h5"
                component="div"
                className="translate"
              >
                Scientific Classification
              </Typography>
              <TableContainer className="table__animal">
                <Table aria-label="simple table" sx={{ md: '80px', xs: '90px' }}>
                  <TableBody className="translate">
                    {props.detailclassification != null
                      ? props.detailclassification.map((val, idx) => {
                        return (
                          <TableRow key={idx}>
                            <TableCell className="td__title--orange" sx={{ width: 'min-content' }}>
                              {val[0].charAt(0).toUpperCase() +
                                val[0].slice(1)}
                            </TableCell>
                            <TableCell className="notranslate">{val[1]}</TableCell>
                          </TableRow>
                        );
                      })
                      : ""}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <Box className="translate" sx={{ marginBottom: '12px' }}>
              <Typography
                sx={{ mt: 4, mb: 2, fontWeight: 600 }}
                variant="h5"
                component="div"
              >
                Locations
              </Typography>
              <List>
                {props.locations?.data.map((item, index) => {
                  return (
                    <ListItem key={index}>
                      <ListItemText
                        className="td__title--orange"
                        sx={{
                          "& >span": { fontWeight: 600 },
                        }}
                      >
                        {item}
                      </ListItemText>
                    </ListItem>
                  );
                })}
              </List>
              <Image
                src={props.locations?.mapUri ?? "/default.png"}
                style={{
                  objectFit: "cover",
                  clipPath: "ellipse(49% 50% at 50% 50%)",
                }}
                width={300}
                height={250}
                alt=""
              />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box >
              <Typography
                sx={{ mt: 4, mb: 2, fontWeight: 600, marginBottom: 0 }}
                variant="h5"
                component="div"
                className="translate"
              >
                {props.detail.replaceAll(/-/g, ' ')?.split(/,|&/g).shift()}
              </Typography>
              <TableContainer>
                <Table aria-label="simple table">
                  <TableBody className="translate">
                    {props.detailFacts != null
                      ? props.detailFacts.map((val, idx) => {
                        return (
                          <TableRow key={idx} >
                            <TableCell className="td__title--orange">
                              {val[0].charAt(0).toUpperCase() +
                                val[0].slice(1)}
                            </TableCell>
                            <ResponsiveTableCell >{val[1]}</ResponsiveTableCell>
                          </TableRow>
                        );
                      })
                      : ""}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          </Grid>
        </Grid>
        <Box
          sx={{
            backgroundColor: "#fff",
            color: "black",
            flexWrap: "wrap",
            textAlign: "center",
            paddingBottom: "20px",
          }}
        >
          <Box
            sx={{
              "& > p": {
                textAlign: "center",
              },
            }}
            className="translate"
          >
            {ReactHtmlParser(props.moredataDetails)}
          </Box>
          <Box
            sx={{
              textAlign: "left",
              paddingLeft: "12px",
              color: "#F56310",
              "&:hover": { fontWeight: 600 },
            }}
          >
            <Link href={props.wikipediaURL ?? "#"} className="translate">More data</Link>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}