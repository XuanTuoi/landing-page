// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination } from "swiper/modules";
// Import Swiper styles
import 'swiper/css';
import "swiper/css/pagination";
import Image from 'next/image';
export default function Slide({ images }) {
  return (
    <Swiper
      spaceBetween={0}
      slidesPerView={1}
      centeredSlides={true}

      autoplay={{
        delay: 2500,
        disableOnInteraction: false,
      }}
      pagination={{
        clickable: true,
      }}
      style={{
        "--swiper-pagination-color": "#F56310",
      }}
      modules={[Autoplay, Pagination]}
    >
      {
        images?.map((value, index) => {
          return <SwiperSlide key={index}>
            <Image loader={() => value}
              src={value ?? '/default.png'}
              fill alt=''
              style={{ objectFit: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat' }}
              loading='lazy'
              placeholder="blur"
              blurDataURL="/default.png"
              unoptimized={true}
            />
          </SwiperSlide>
        })
      }

    </Swiper>
  );
};