import { Box, Typography, useMediaQuery, useTheme } from "@mui/material";
import { Roboto } from "next/font/google";
import Link from "next/link";
import React from "react";

import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import { useRouter } from "next/router";
import { listApp, listAnimal } from "../contants";
import { useTranslation } from "next-i18next";
const RobotoFont = Roboto({
  subsets: ["latin"],
  weight: ["400", "500", "700"],
});

const Footer = () => {
  const theme = useTheme();
  const router = useRouter();
  const { t: translate } = useTranslation("common");
  const app = listApp.filter((item) => item.link === router.query.app)[0];
  const animalName = listAnimal.filter(
    (item) => item.link === router.query.name
  )[0];
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  if (isMobile)
    return (
      <Box
        className={RobotoFont.className}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        {router.pathname === "/" && (
          <Box
            sx={{
              backgroundColor: "#fff",
              padding: "53px",
              width: "100%",
            }}
          >
            <Typography
              sx={{
                fontSize: "26px",
                textAlign: "center",
                marginBottom: "40px",
                color: "#333",
              }}
            >
              {translate("footer.social")}
            </Typography>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
                width: "100%",
                "& a": {
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                },
                "& svg": {
                  fontSize: "40px",
                  marginBottom: "10px",
                },
                "& p": {
                  fontSize: "18px",
                  color: "#666",
                },
              }}
            >
              <Link href='#'>
                <FacebookIcon />
                <Typography>Facebook</Typography>
              </Link>
              <Link href='#'>
                <InstagramIcon />
                <Typography>Instagram</Typography>
              </Link>
            </Box>
          </Box>
        )}

        <Box
          sx={{
            backgroundColor: "#262626",
            color: "#fff",
            padding: "53px 0",
            width: "100%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            "& a,p": {
              fontSize: "18px",
              color: "#999",
              marginBottom: "20px",
            },
          }}
        >
          <Typography
            variant='h1'
            sx={{
              fontSize: "26px",
              fontWeight: "500",
              color: "#fff",
              marginBottom: "40px",
            }}
          >
            {translate("footer.pt")}
          </Typography>
          <Link href='/policy'>{translate("footer.pp")}</Link>
          <Link href='/term'>{translate("footer.tos")}</Link>
        </Box>
        <Typography
          variant='h4'
          sx={{
            backgroundColor: "#262626",
            color: "#999",
            fontSize: "12px",
            padding: "22px",
            width: "100%",
            textAlign: "center",
            borderTop: "1px solid #ccc",
          }}
        >
          Copyright © Next Vision Limited. All Rights Reserved.
        </Typography>
      </Box>
    );
  return (
    <Box
      className={RobotoFont.className}
      sx={{
        backgroundColor: "#262626",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: { xs: "column", md: "row" },
          justifyContent: "space-between",
          alignItems: "flex-start",
          color: "#fff",
          padding: { xs: "40px 0", sm: "20px", md: "70px 20px 100px 0" },
          "& > div": {
            display: "flex",
            flexDirection: "column",
            alignItems: { xs: "center", md: "flex-start" },
            justifyContent: "center",
            margin: { sm: "20px 0", md: "0 20px", lg: "0 60px" },
            width: { sm: "100%" },
            "& p": {
              fontSize: { xs: "26px", md: "22px" },
              fontWeight: "400",
              color: { xs: "#fff", md: "#ccc" },
              marginBottom: { sm: "10px", md: "20px" },
              whiteSpace: "nowrap",
            },
            "& a": {
              fontSize: { xs: "18px", md: "16px" },
              fontWeight: { xs: "500", md: "400" },
              margin: { xs: "16px 0", sm: "8px 0", md: "10px 0" },
              color: { xs: "#999", md: "#fff" },
              transition: "all 0.2s ease-in-out",
              "&:hover": {
                color: "#F56310",
              },
            },
            "& span": {
              fontSize: "18px",
              display: "block",
              margin: { xs: "16px 0", md: "10px 0" },
              color: { xs: "#999", md: "#fff" },
            },
          },
        }}
      >
        <Box>
          <Typography>{translate("footer.social")}</Typography>
          {app?.listSocial.map((item, index) => {
            if (item.link) {
              return (
                <Link key={index} href={item?.link || ""} target='_blank'>
                  {item?.title}
                </Link>
              );
            }
            return "";
          }) ??
            animalName?.listSocial.map((item, index) => {
              return (
                <Link key={index} href={item?.link || ""} target='_blank'>
                  {item?.title}
                </Link>
              );
            })}
        </Box>
        <Box>
          <Typography>{translate("footer.pt")}</Typography>

          <Link href='/policy'>{translate("footer.pp")}</Link>
          <Link href='/term'>{translate("footer.tos")}</Link>
        </Box>
        <Box
          sx={{
            display: isMobile ? "none !important" : "block",
          }}
        >
          <Typography>{translate("footer.hs")}</Typography>
          <Link href='/faq'>FAQ</Link>
          <Typography variant='h7'>
            {app?.linkContact ?? animalName?.linkContact}
          </Typography>
        </Box>
        <Box
          sx={{
            display: isMobile ? "none !important" : "block",
          }}
        >
          <Typography>{translate("footer.download")}</Typography>
          {app?.linkAndroid || animalName?.linkAndroid ? (
            <Link
              href={app?.linkAndroid ?? animalName?.linkAndroid}
              target='_blank'
            >
              Android
            </Link>
          ) : (
            <Typography variant='h7'>Android</Typography>
          )}
          {app?.linkIos || animalName?.linkIos ? (
            <Link href={app?.linkIos ?? animalName?.linkIos} target='_blank'>
              iOs
            </Link>
          ) : (
            <Typography variant='h7'>iOs</Typography>
          )}
        </Box>
      </Box>
      <Typography
        variant='h4'
        sx={{
          color: "#999",
          fontSize: "12px",
          padding: "22px",
          width: "100%",
          textAlign: "center",
          borderTop: "2px solid #ccc",
        }}
      >
        Copyright © Next Vision Limited. All Rights Reserved.
      </Typography>
    </Box>
  );
};

export default Footer;
