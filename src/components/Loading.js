import { Box } from "@mui/material";

const IsLoading = () => {
    return (
        // eslint-disable-next-line react/react-in-jsx-scope
        <Box sx={{
            height: '800px',
            backgroundColor: '#fff',
            color: '#000'
        }}
        />          
    )
}

export default IsLoading