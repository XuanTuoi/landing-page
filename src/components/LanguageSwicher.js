import { useEffect } from "react"
import Cookies from 'js-cookie';
import { useRouter } from "next/router"
const COOKIE_NAME = "googtrans"
const LanguageSwitcher = ({locale}) => {
  if (locale == 'vn') {
    locale = 'vi';
  }
  const router = useRouter()

  useEffect(() => {
    const switchLanguage = (newLocale) => {
      const oldCookie = Cookies.get(COOKIE_NAME);
      if (oldCookie) {
        const prod = process.env.NODE_ENV === 'production';
        if (prod) {
          Cookies.remove(COOKIE_NAME, { path: '/', domain: '.snapapps.online' });
        } else {
          Cookies.remove(COOKIE_NAME);
        }
      }

      if ((newLocale === 'en' && !oldCookie?.includes('/auto/') && oldCookie?.includes('/en')) || !oldCookie?.includes(newLocale)) {
        Cookies.set(COOKIE_NAME, "/auto/" + newLocale);
        if (newLocale === 'vi') {
          newLocale = 'vn';
        }
        window.location.replace(`/${newLocale}${router.asPath}`);
      }
    };
    switchLanguage(locale);
  }, [locale,router]);
}

export { LanguageSwitcher, COOKIE_NAME }
