import {
  Box,
  MenuItem,
  Select,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import { Roboto } from 'next/font/google';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { countryName, listApp, listAnimal } from '../contants';
import ModalDownload from './Modal';
import { useTranslation } from 'next-i18next';
//icon
import DensityMediumIcon from '@mui/icons-material/DensityMedium';
import ModalMenu from './ModalMenu';
import { getAppName } from '../ultils';
import { LanguageSwitcher } from './LanguageSwicher';
import Script from 'next/script';

const RobotoFont = Roboto({
  subsets: ['latin'],
  weight: ['400', '500', '700'],
});

const Header = () => {
  const { locale, locales, push } = useRouter();
  const [appLanguage, setAppLanguage] = React.useState(locale);
  
  const { t: translate } = useTranslation('common');
  const [itemForDownload, setItemForDownload] = React.useState({});
  const [open, setOpen] = React.useState(false);
  const [openMenu, setOpenMenu] = React.useState(false);

  const router = useRouter();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const handleOpen = (item) => {
    setOpen(true);
    setItemForDownload(item);
  };
  const handleClose = () => setOpen(false);
  const handleChange = (event) => {
    setAppLanguage(event.target.value);
  };

  const [itemRender, setItemRender] = React.useState({});

  useEffect(() => {
    if (appLanguage !== locale) { 
      push(router.asPath, router.asPath, { locale: appLanguage });
    }
  }, [appLanguage]);

  useEffect(() => {
    listApp.map((item) => {
      if (router.asPath.includes(getAppName(item.link))) {
        setItemRender(item);
      }
    });
  }, [router]);
  return (
    <Box
      className={RobotoFont.className}
      sx={{
        padding: '10px',
        position: 'fixed',
        zIndex: '999',
        backgroundColor: '#fff',
        color: '#000',
        width: '100%',
        height: {
          xs: 'var(--header-height-xs)',
          sm: 'var(--header-height-md)',
        },
        boxShadow: '0 0 5px rgba(0,0,0,0.1)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      {isMobile ? (
        <Box
          sx={{
            cursor: 'pointer',
            width: '100%',
          }}
        >
          <Box
            onClick={() => setOpenMenu(true)}
            sx={{
              display: 'flex',
              alignItems: 'center',
              '& > svg': {
                marginLeft: 'auto',
              },
            }}
          >
            {router.pathname !== '/' && (
              <Box
                sx={{
                  marginRight: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                <Box
                  sx={{
                    width: '60px',
                    height: '60px',
                    position: 'relative',
                    '& > img': {
                      objectFit: 'cover',
                    },
                  }}
                >
                  <Image
                    fill
                    src={itemRender.logoPath}
                    alt={itemRender?.name}
                  />
                </Box>
                <Typography
                  variant="h2"
                  sx={{
                    fontWeight: '700',
                    fontSize: '32px',
                    marginLeft: '10px',
                  }}
                >
                  {itemRender?.name}
                </Typography>
              </Box>
            )}
            <DensityMediumIcon />
          </Box>
          <ModalMenu
            itemForDownload={itemForDownload}
            handleOpen={handleOpen}
            open={open}
            setOpen={setOpen}
            openMenu={openMenu}
            setOpenMenu={setOpenMenu}
            appLanguage={appLanguage}
            setAppLanguage={setAppLanguage}
          />
        </Box>
      ) : (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '1200px',
          }}
        >
          {router?.pathname !== '/' && (
            <Box
              sx={{
                marginRight: 'auto',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box
                sx={{
                  width: '60px',
                  height: '60px',
                  position: 'relative',
                  '& > img': {
                    objectFit: 'cover',
                  },
                }}
              >
                <Image
                  fill
                  src={itemRender?.logoPath}
                  alt={itemRender?.name || 'img'}
                />
              </Box>
              <Typography
                variant="h2"
                sx={{
                  fontWeight: '700',
                  fontSize: { xs: '24px', md: '32px' },
                  marginLeft: '10px',
                }}
              >
                {itemRender?.name}
              </Typography>
            </Box>
          )}

          {router.pathname === '/term' && (
            <Typography
              sx={{
                fontWeight: '500',
                fontSize: '18px',
                textTransform: 'uppercase',
              }}
            >
              {translate('footer.tos')}
            </Typography>
          )}
          {router.pathname === '/policy' && (
            <Typography
              sx={{
                fontWeight: '500',
                fontSize: '18px',
                textTransform: 'uppercase',
              }}
            >
              {translate('footer.pp')}
            </Typography>
          )}

          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-around',
              marginLeft: 'auto',
              '& >  a': {
                padding: '0 20px',
                fontWeight: '500',
                fontSize: '18px',
                cursor: 'pointer',
                transition: 'all 0.2s ease',
                '&:hover': { color: '#F56310' },
              },
            }}
          >
            <Typography
              sx={{
                padding: '0 20px',
                fontWeight: `${router.asPath == '/' ? '600' : '500'}`,
                fontSize: '18px',
                cursor: 'pointer',
                '&:hover': { color: '#F56310' },
              }}
            >
              <Link href="/" style={{}}>
                {translate('home')}
              </Link>
            </Typography>

            <Box
              sx={{
                position: 'relative',

                '&:hover': {
                  '& > div': {
                    display: 'flex',
                  },
                },
              }}
            >
              <Typography
                sx={{
                  padding: '0 20px',
                  fontWeight: `${
                    router.asPath.includes('animal') ? '600' : '500'
                  }`,
                  fontSize: '18px',
                  cursor: 'pointer',
                  '&:hover': { color: '#F56310' },
                }}
              >
                {translate('animal')}
              </Typography>

              <Box
                sx={{
                  width: 'max-content',
                  position: 'absolute',
                  top: '100%',
                  left: '0px',
                  alignItems: 'center',
                  flexDirection: 'column',
                  backgroundColor: '#fff',
                  boxShadow: '0 0 5px rgba(0,0,0,0.2)',
                  borderRadius: '5px',
                  padding: '0',
                  marginTop: '10px',
                  display: 'none',
                  '&::before': {
                    content: '""',
                    position: 'absolute',
                    top: '-16px',
                    left: '0',
                    width: '100%',
                    height: '30px',
                  },
                  '& a': {
                    fontWeight: '400',
                    padding: '10px 35px',
                    '&:hover': {
                      color: '#F56310',
                    },
                  },
                }}
              >
                {listAnimal.map((item, index) => {
                  return (
                    <Link
                      href={`/animal/${item?.link}` || ''}
                      key={index}
                      style={{
                        fontWeight: `${
                          router.asPath.includes('animal/' + item?.link)
                            ? '600'
                            : '400'
                        }`,
                      }}
                    >
                      {translate(item?.name)}
                    </Link>
                  );
                })}
              </Box>
            </Box>
            <Box
              sx={{
                position: 'relative',

                '&:hover': {
                  '& > div': {
                    display: 'flex',
                  },
                },
              }}
            >
              <Typography
                sx={{
                  padding: '0 20px',
                  fontWeight: `${
                    router.asPath.includes('/application') ? '600' : '500'
                  }`,
                  fontSize: '18px',
                  cursor: 'pointer',
                  '&:hover': { color: '#F56310' },
                }}
              >
                {translate('application')}
              </Typography>

              <Box
                sx={{
                  position: 'absolute',
                  top: '100%',
                  left: '0px',
                  alignItems: 'center',
                  flexDirection: 'column',
                  backgroundColor: '#fff',
                  boxShadow: '0 0 5px rgba(0,0,0,0.2)',
                  borderRadius: '5px',
                  padding: '0',
                  marginTop: '10px',
                  display: 'none',
                  '&::before': {
                    content: '""',
                    position: 'absolute',
                    top: '-16px',
                    left: '0',
                    width: '100%',
                    height: '30px',
                  },
                  '& a': {
                    fontWeight: '400',
                    padding: '10px 20px',
                    '&:hover': {
                      color: '#F56310',
                    },
                  },
                }}
              >
                {listApp.map((item, index) => {
                  return (
                    <Link
                      href={`/application/${item?.link}` || ''}
                      key={index}
                      style={{
                        fontWeight: `${
                          router.asPath.includes(item?.link) ? '600' : '400'
                        }`,
                      }}
                    >
                      {item?.name}
                    </Link>
                  );
                })}
              </Box>
            </Box>

            <Box
              sx={{
                position: 'relative',
                '&:hover': {
                  '& > div': {
                    display: 'flex',
                  },
                },
              }}
            >
              <Typography
                sx={{
                  padding: '0 20px',
                  fontWeight: '500',
                  fontSize: '18px',
                  cursor: 'pointer',
                  '&:hover': { color: '#F56310' },
                }}
              >
                {translate('download')}
              </Typography>

              <Box
                sx={{
                  position: 'absolute',
                  top: '100%',
                  left: '0px',
                  alignItems: 'center',
                  flexDirection: 'column',
                  backgroundColor: '#fff',
                  boxShadow: '0 0 5px rgba(0,0,0,0.2)',
                  borderRadius: '5px',
                  padding: '0',
                  marginTop: '10px',
                  display: 'none',
                  '&::before': {
                    content: '""',
                    position: 'absolute',
                    top: '-16px',
                    left: '0',
                    width: '100%',
                    height: '30px',
                  },
                  '& a': {
                    fontWeight: '400',
                    padding: '10px 20px',
                  },
                }}
              >
                {listApp.map((item, index) => {
                  return (
                    <Box key={index}>
                      <Box
                        sx={{
                          padding: '10px 20px',
                          fontWeight: '400',
                          cursor: 'pointer',
                          '&:hover': { color: '#F56310' },
                        }}
                        onClick={() => handleOpen(item)}
                      >
                        {item.name}
                      </Box>
                      <ModalDownload
                        open={open}
                        handleClose={handleClose}
                        itemForDownload={itemForDownload}
                      />
                    </Box>
                  );
                })}
              </Box>
            </Box>
            <LanguageSwitcher locale={locale}/>
            <Select
              sx={{
                padding: '0 !important',
                '& input, fieldset': {
                  border: 'none',
                },
              }}
              value={appLanguage}
              onChange={handleChange}
              autoWidth
            >
              {locales.map((item, index) => {
                return (
                  <MenuItem
                    value={item}
                    key={index}
                    onClick={() => setAppLanguage(item)}
                  >
                    {countryName(item)}
                  </MenuItem>
                );
              })}
            </Select>
          </Box>
        </Box>
      )}
      <Script src="/assets/scripts/lang-config.js" strategy="beforeInteractive"  />
      <Script src="/assets/scripts/translation.js" strategy="beforeInteractive" />
      <Script src="//translate.google.com/translate_a/element.js?cb=TranslateInit" strategy="afterInteractive" />
    </Box>
  );
};

export default Header;
