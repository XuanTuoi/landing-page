export const listApp = [
  {
    id: 1,
    name: "CoinID",
    appName: "CoinID: Snap & Identify Coins",
    description:
      "The Coin Identifier app offers a range of features that make it an ideal tool for identifying coin.",
    link: "coinid",
    linkIos:
      "https://apps.apple.com/us/app/coinid-snap-identify-coins/id6473073014",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.coinidentifier&pcampaignid=web_sharebirdaidetect&pcampaignid=web_sharepcampaignid%3Dweb_shareid%3Dweb_sharem.stoneaidetectg.insectAI&pli=1",
    logoPath: "/coin.png",
    listSocial: [
      {
        title: "Tiktok",

        // link: "https://www.tiktok.com/@insectid",
      },
      {
        title: "Instagram",
        // link: "https://www.instagram.com/insectID911",
      },
      {
        title: "Youtube",
        // link: "https://www.youtube.com/@InsectID911/featured",
      },
      {
        title: "Facebook",
        // link: "https://www.facebook.com/insectID911/",
      },
    ],
    linkContact: "coinid@ikong.vn",
    linkPolicy: "https://coin.snapapps.online/coin/policy",
    linkTerm: "https://coin.snapapps.online/coin/terms",
  },
  {
    id: 2,
    name: "Insect ID",
    appName: "Insect ID: Bug AI",
    description:
      "The Bug Identifier app offers a range of features that make it an ideal tool for identifying insects.",
    link: "insectid",
    linkIos: "https://apps.apple.com/vn/app/ai-insect-detect/id1673704832",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.ikong.insectAI",
    logoPath: "/insect.png",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@insectid",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/insectID911",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@InsectID911/featured",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/insectID911/",
      },
    ],
    linkContact: "insectid@ikong.vn",
    linkPolicy: "https://insect.snapapps.online/insect/policy",
    linkTerm: "https://insect.snapapps.online/insect/terms",
  },
  {
    id: 3,
    name: "Plant App",
    appName: "Plant App - Identifier & Care",
    description:
      "Plant App is an optimized application for identifying plants and learning more about the plant kingdom around you.",
    link: "plantid",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.findplant.plantidentification",
    linkIos: "https://apps.apple.com/us/app/ecological-ai/id6449490403",
    logoPath: "/plant.webp",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@plantid_plant_app",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/plantid_plant_identify/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/channel/UCcbOxOjRAgrGXVIjKJtFZ1A",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/findplant.net",
      },
    ],
    linkContact: "plantid@ikong.vn",
    linkPolicy: "https://sites.google.com/view/plantlover-termandpolicy/view",
    linkTerm: "https://sites.google.com/view/terms-conditions-form",
  },
  {
    id: 4,
    name: "MushroomAI",
    appName: "MushroomAI: Fungi ID & Guide",
    description:
      "MushroomAI: Fungi ID & Guide is the ultimate mobile app for mushroom enthusiasts, foragers, and anyone who wants to learn more about the fascinating world of mushrooms.",
    link: "mushroomai",
    linkIos: "https://apps.apple.com/us/app/id6447349561",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.mushroomaidetect&hl=en&gl=us",
    logoPath: "/mushroom.png",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@mushroom.id.01",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/mushroom.ai01/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@MushroomAI/featured",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/mushroomai/",
      },
    ],
    linkContact: "mushroomid@ikong.vn",
    linkPolicy: "https://mushroom.snapapps.online/mushroom/policy",
    linkTerm: "https://mushroom.snapapps.online/mushroom/terms",
  },
  {
    id: 5,
    name: "RockSpot",
    link: "rockspot",
    appName: "RockSpot: Find Stones & Gems",
    description:
      "RockSpot is the ultimate guide for anyone interested in the fascinating world of geology. ",
    logoPath: "/stone.png",
    linkIos: "https://apps.apple.com/us/app/stoneaidetect/id6448445038",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.stoneaidetect",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@stone.id01",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/stone_ai01/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@StoneID",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/rockspotID/",
      },
    ],
    linkContact: "stoneid@ikong.vn",
    linkPolicy: "https://stone.snapapps.online/stone/policy",
    linkTerm: "https://stone.snapapps.online/stone/terms",
  },
  {
    id: 6,
    name: "FishDetect",
    link: "fishdetect",
    appName: "FishDetect - Fish Identifier",
    description:
      "FishDetect is a breakthrough mobile app designed to provide you with an exceptional fish identification experience.",
    logoPath: "/fish.webp",
    linkIos: "https://apps.apple.com/us/app/fish-ai/id6454900761",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.fishaidetect",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@fishdetect.ai",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/fishdetect.ai/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@FishDetectAI01",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/profile.php?id=61550506806652",
      },
    ],
    linkContact: "fishdetect.ai@ikong.vn",
    linkPolicy: "https://fish.snapapps.online/fish/policy",
    linkTerm: "https://fish..snapapps.online/fish/terms",
  },
  {
    id: 7,
    name: "Bird Detect",
    link: "birddetect",
    appName: "Bird Detect",
    description:
      "Identify birds accurately and quickly using various tools and techniques.",
    logoPath: "/bird.webp",
    linkIos: "https://apps.apple.com/us/app/bird-detect/id6460856643",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.birdaidetect",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@birddetect.ai",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/birddetect01/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@BirdDetectAI01",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/profile.php?id=61550492977786",
      },
    ],
    linkContact: "birddetect.ai@ikong.vn",
    linkPolicy: "https://bird.snapapps.online/bird./policy",
    linkTerm: "https://bird..snapapps.online/bird./terms",
  },
];

export const countryName = (locale) => {
  switch (locale) {
    case "en":
      return "English";
    case "vn":
      return "Vietnam";
    case "ja":
      return "Japan";
    case "ko":
      return "Korean";
    case "pt":
      return "Portugu";
    case "es":
      return "Spanish";
    case "ar":
      return "Arabic";
  }
};

export const listAnimal = [
  {
    id: 1,
    name: "Fish",
    link: "fish",
    logoPath: "/fish.webp",
    linkIos: "https://apps.apple.com/us/app/fish-ai/id6454900761",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.fishaidetect",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@fishdetect.ai",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/fishdetect.ai/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@FishDetectAI01",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/profile.php?id=61550506806652",
      },
    ],
    linkContact: "fishdetect.ai@ikong.vn",
    linkPolicy: "https://fish.snapapps.online/fish/policy",
    linkTerm: "https://fish..snapapps.online/fish/terms",
  },
  {
    id: 2,
    name: "Bird",
    link: "bird",
    logoPath: "/bird.webp",
    linkIos: "https://apps.apple.com/us/app/bird-detect/id6460856643",
    linkAndroid:
      "https://play.google.com/store/apps/details?id=com.birdaidetect",
    listSocial: [
      {
        title: "Tiktok",
        link: "https://www.tiktok.com/@birddetect.ai",
      },
      {
        title: "Instagram",
        link: "https://www.instagram.com/birddetect.ai/",
      },
      {
        title: "Youtube",
        link: "https://www.youtube.com/@BirdDetectAI01",
      },
      {
        title: "Facebook",
        link: "https://www.facebook.com/profile.php?id=61550492977786",
      },
    ],
    linkContact: "birddetect.ai@ikong.vn",
    linkPolicy: "https://bird.snapapps.online/fish/policy",
    linkTerm: "https://bird..snapapps.online/fish/terms",
  },
];
