import React, { useState, useEffect, Suspense } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { PageFetchAnimalApi } from "@/src/pages/api/index"
import PagesAnimal from "@/src/components/AnimalComponent/PageAnimal";
import IsLoading from "@/src/components/Loading";
import { Box } from "@mui/material";
import { useSearchParams, } from "next/navigation";
export default function Animal({ name, page }) {
  const searchParams = useSearchParams();
  const [searchQuery, setSearchQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const searchQ = searchParams.get('searchQ');
  useEffect(() => {
    page ? setCurrentPage(page) : setCurrentPage(1);
    searchQ ? setSearchQuery(searchQ) : setSearchQuery("");
  }, [page, searchQ]);
  const { data, isLoading } = PageFetchAnimalApi(name, searchQuery, currentPage);
  if (isLoading) return <IsLoading></IsLoading>
  return (
    <Box
      sx={{
      backgroundColor: "#fff",
      padding: "0px 12px",
      minHeight:'800px'
      }}>
      <Suspense fallbackData={'Loading...'}>
      <PagesAnimal currentPage={currentPage} data={data}
          name={name} search={searchQ}
          setCurrentPage={setCurrentPage}
          />
      </Suspense>
    </Box>
  );
};

export async function getServerSideProps({ locale, res, params, query }) {
  const name = params.name;
  const page = parseInt(query.page);
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'app'])),
      // Will be passed to the page component as props
      name,
      page,
    },
  };
}