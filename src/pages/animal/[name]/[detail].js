"use client";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { DetailFetchAnimalApi } from "@/src/pages/api/index";
import DetailAnimalComponent from "@/src/components/AnimalComponent/DetailAnimal";

export default function DetailAnimal() {
  const router = useRouter();

  const { name, detail } = router.query;
  const detailName = detail.replaceAll(/-/g, " ");
  const data = DetailFetchAnimalApi(name, detailName);
  const {
    classification,
    images,
    locations,
    facts,
    moreData,
    wikipediaURL,
  } = data?.datas[0] ?? {};

  const moredataDetails = moreData
    ?.substring(0, moreData?.indexOf("View all"))
    ?.replace(/href=".*?"/g, "");
  let detailFacts;
  let detailclassification;
  if (facts != null && classification != null) {
    detailFacts = Object.entries(facts);
    detailclassification = Object.entries(classification);
  }
  return (
      <DetailAnimalComponent
      images={images}
      locale={router.locale}
      facts={facts}
      detailclassification={detailclassification}
      detail={detail}
      detailFacts={detailFacts}
      classification={classification}
      locations={locations}
      moredataDetails={moredataDetails}
      wikipediaURL={wikipediaURL}
    />
  );
}

export async function getServerSideProps({ locale, params, query }) {
  const name = params.name;
  const page = parseInt(query.page);
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common", "app"])),
      // Will be passed to the page component as props
      name,
      page,
    },
  };
}
