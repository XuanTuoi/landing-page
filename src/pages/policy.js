/* eslint-disable react/prop-types */
import React from 'react';
import { Box, Typography } from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

const Policy = () => {
  const { t: translate } = useTranslation('policy');

  const Subtitle = ({ number }) => {
    return (
      <Typography variant="h3">
        {translate(`${number}.title`)}

        <Typography
          sx={{
            display: 'inline',
          }}
        >
          &nbsp;
          {translate(`${number}.content`)}
        </Typography>
      </Typography>
    );
  };

  const ListText = ({ texts, type }) => {
    if (type === 'star') {
      return texts.map((item, index) => {
        return (
          <Typography key={index}> {translate(`text${index}`)}</Typography>
        );
      });
    }
    return texts.map((item, index) => {
      return <Typography key={index}>{translate(`item${index}`)}</Typography>;
    });
  };

  return (
    <Box
      sx={{
        backgroundColor: '#ffffff',
        color: '#4a4a4a',
      }}
    >
      {/* Content */}
      <Box
        sx={{
          background: '#f8f9fc',
          padding: { xs: '20px' },
        }}
      >
        <Box
          sx={{
            maxWidth: '1220px',
            margin: '0 auto',
            fontSize: '1px',
            '& p': {
              lineHeight: '32px',
            },
            '& h2': {
              fontSize: '16px',
              fontWeight: '600',
              lineHeight: '32px',
              color: '#363636',
            },
            '& h3': {
              fontSize: '16px',
              fontWeight: '600',
              lineHeight: '32px',
              color: '#363636',
            },
          }}
        >
          <Typography> &nbsp;{translate('intro')}</Typography>
          <Subtitle
            number="1"
            title="1. GENERAL"
            content={`Regarding the Services: ( "we", "us" or "our") is the Data Controller within the meaning of the General Data Protection Regulation ("GDPR"). This privacy policy shall inform you on how we collect, process and use ("Use") personal data in connection with the Services.`}
          />
          <Typography variant="h2">{translate('2')}</Typography>
          <Subtitle
            number="2_1"
            title="2.1 In general"
            content={`you can use the Services without providing any personal information such as e.g. your name, email address, postal address, telephone number, financial information (all such information concerning the personal or material circumstances of an identified or identifiable individual data subject together "Personal Data"). Therefore, if you do not provide us directly with Personal Data in another way or actively consent to the Use by us of certain Personal Data, we, in general, do not Use your data with the exception of the following: 2.1.1 Regarding the Website: your browser transfers certain data so that it can access the Website, namely: the IP address, the date and time of the request, the browser type, the operating system, the language and version of the browser software.`}
          />
          <Subtitle
            number="2_1_1"
            title="2.1.1 Regarding the Website:"
            content={`your browser transfers certain data so that it can access the Website, namely: the IP address, the date and time of the request, the browser type, the operating system, the language and version of the browser software.`}
          />
          <Subtitle
            number="2_1_2"
            title="2.1.2 Regarding the App:"
            content={`certain data is transmitted by your mobile device: namely your IP address, the periods of time in which you use the App and the type of device you use (e.g. IPhone, IPad), as well as events within the application recorded with Amplitude analytics in order to give us the possibility to continuously improve the App.`}
          />
          <Subtitle
            number="2_1_3"
            title="2.1.3 Regarding the App:"
            content={`in certain cases, such as downloading and installing a free trial of a paid App, we ask you to enter a valid email address to begin your free trial.`}
          />
          <Subtitle
            number="2_1_4"
            title="2.1.4 Regarding the App:"
            content={`certain apps allow you to send documents via Fax. In order for this functionality to work we need to record a fax number and the document intended for transfer. Once the document is sent, the fax number along with the document will be removed after 30 days.`}
          />
          <Subtitle
            number="2_1_5"
            title="2.1.5 Regarding the email:"
            content={`we save a record of communication including attachments and information you voluntary decide to share with us for troubleshooting purposes whenever you communicate with our support team.`}
          />
          <Subtitle
            number="2_1_6"
            title="2.1.6 Email messages"
            content={`sent by us via third-party services like Mailgun may contain tracking pixel which helps us collect statistics on delivery and opening rates of our correspondence. These pixels do not provide us with any additional personal data about you or your behavior online. You can disable image rendering in your email client which will deactivate this feature, however you will be unable to see any images within other received emails. The legal basis for the processing of the data described above in section 2.1.1 through 2.1.6 (to the extent such data is to be considered Personal Data) is Art. 6 (1) sentence 1 lit. f GDPR (legitimate interests; regarding section 2.1.1 the legitimate interests to Use such data arises from the fact that without such data the Website cannot be accessed by our customers and/or other users; regarding section 2.1.2 the legitimate interests to Use such data is that we have an interest to continuously improve the App.`}
          />
          <Subtitle
            number="2_2"
            title="2.2"
            content={` Only in cases in which you have given us prior consent we also use your Personal Data for other purposes but only to the extent needed in each specific case. The legal basis for the processing of the data described in this section 2.2 is Art. 6 (1) sentence 1 lit. a GDPR (consent).`}
          />
          <Subtitle
            number="2_3"
            title="2.3"
            content={`We may disclose Personal Data to third parties where such disclosure is required by law (for example, upon request of a court or of law enforcement authorities). The legal basis for the processing of the data described in this section 2.3 is Art. 6 (1) sentence 1 lit. c GDPR (legal obligation).`}
          />
          <Subtitle
            number="2_4"
            title="2.4"
            content={` We may also Use certain data as set forth in section 3 below.`}
          />
          <Typography variant="h2">{translate('3')}</Typography>
          <Subtitle
            number="3_1"
            title="3.1 Subscription Information:"
            content={`12 Months Subscription`}
          />
          <ListText
            type="star"
            texts={[
              '* Subscription with a free trial period will automatically renew to a paid subscription.',
              '* Please note: any unused portion of a free trial period (if offered) will be forfeited when you purchase a premium subscription during the free trial period.',
              '* You can cancel a free trial or subscription anytime canceling your subscription through your iTunes account settings. This must be done 24 hours before the end of a free trial or subscription period to avoid being charged. The cancellation will take effect the day after the last day of the current subscription period, and you will be downgraded to the free service.',
            ]}
          />
          <Typography variant="h2">{translate('3_2')}</Typography>
          <ListText
            type="line"
            texts={[
              '– Payment will be charged to your iTunes Account at confirmation of purchase',
              '– Your subscription will automatically renew unless auto-renew is turned off at least 24-hours before the end of the current period',
              '– Your account will be charged for renewal within 24-hours prior to the end of the current period and the cost will be identified.',
              '– Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user’s Account Settings after purchase',
            ]}
          />
        </Box>
      </Box>

      <Typography
        variant="h5"
        sx={{
          textAlign: 'center',
          padding: '32px 0',
          fontSize: '16px',
        }}
      >
        Copyright © Next Vision Limited. All Rights Reserved.
      </Typography>
    </Box>
  );
};

export default Policy;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'policy'])),
      // Will be passed to the page component as props
    },
  };
}
