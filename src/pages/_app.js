/* eslint-disable react/prop-types */
import '@/styles/globals.css';
import Aos from 'aos';
import React, { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import 'aos/dist/aos.css';
import { appWithTranslation } from 'next-i18next';

function App({ Component, pageProps }) {
  useEffect(() => {
    Aos.init({
      duration: 2000,
    });
  }, []);

  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default appWithTranslation(App);
