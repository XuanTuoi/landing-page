import { Box, Typography } from '@mui/material';
import React from 'react';
import AccordionList from '../components/AccordionList';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
const FAQ = () => {
  const { t: translate } = useTranslation('common');

  return (
    <Box
      sx={{
        paddingBottom: '100px',
      }}
    >
      <Typography
        variant="h4"
        sx={{
          textAlign: 'center',
          backgroundColor: '#f0f2f5',
          color: '#333',
          padding: { xs: '60px 0', md: '80px 0' },
          fontSize: { xs: '32px', md: '40px' },
        }}
      >
        {translate('faqs.title')}
      </Typography>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          padding: { xs: '16px' },
          margin: '0 auto',
        }}
        data-aos="fade-down"
      >
        <AccordionList
          i="0"
          title="Using Apps AI Detect"
          listAccordions={[
            {
              key: '0',
              title:
                'Does Picture only accept newly-taken pictures or can I select pictures from my album?',
              description: [
                `Both methods work on our apps detect. You can either take a new picture of the or choose one from your gallery. On the photo page, tap the camera icon to take a new picture with your camera or tap ''Photos'' at the bottom left of the screen to select a picture from your album.`,
              ],
            },

            {
              key: '1',
              title:
                'One of the identifications is not accurate, what should I do?',
              description: [
                `Since the app relies on a single picture to identify, sometimes changing the angle or distance a bit can help and please make sure the camera is focused on the species you want to identify.`,
              ],
            },
            {
              key: '2',
              title:
                "I can't find the species I want to identify, what should I do?",
              description: [
                `We are constantly adding new species to our database. If you can't find the species you want to identify, please send us a message and we will add it to our database as soon as possible.`,
              ],
            },
            {
              key: '3',
              title: 'Application loading slowly, what should I do?',
              description: [
                `Please check your internet connection or switch to another network. If the problem persists, please contact us.`,
              ],
            },
            {
              key: '4',
              title: 'How to cancel my subscription?',
              description: [
                'You can cancel your subscription in the App Store/Google Play Store. For more information, please refer to the following links: https://support.apple.com/en-us/HT202039 for Ios or https://support.google.com/googleplay/answer/7018481?co=GENIE.Platform%3DAndroid&hl=en for Android',
              ],
            },
          ]}
        />

        <AccordionList
          i="1"
          title="Others"
          listAccordions={[
            {
              key: '5',
              title: 'Some function is not working, what should I do?',
              description: [
                `Re-installing the app usually solves most technical issues. The account is bound with your Apple ID/Google account, so it’s safe to re-install. `,
              ],
            },
            {
              key: '6',
              title: "How can I send my feedback to Apps AI Detect's team?",
              description: [
                `We are always happy to hear from you. You can choose setting page and tap “Feedback” to send us your feedback.`,
              ],
            },
            {
              key: '7',
              title: "Do I need to pay for using Apps AI Detect's services?",
              description: [
                `You have 3 days free trial to use our services. After that, if you want to continue using our services, you need to pay for it. You can choose to pay weekly, monthly or yearly.`,
              ],
            },
          ]}
        />
      </Box>
    </Box>
  );
};

export default FAQ;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['faq', 'common'])),
      // Will be passed to the page component as props
    },
  };
}
