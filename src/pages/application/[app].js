import ModalDownload from '@/src/components/Modal';
import WrapQRcode from '@/src/components/WrapQRcode';
import { listApp } from '@/src/contants';
import { Box, Button, Typography } from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';

const Application = () => {
  const router = useRouter();
  const [open, setOpen] = React.useState(false);

  const app = listApp.filter((item) => item.link === router.query.app)[0];

  const { t: translate } = useTranslation('app');
  const handleClose = () => setOpen(false);
  // https://snapapps.sfo3.digitaloceanspaces.com/insect/post/1696995116502_kytu.jpg
  // http://localhost:3000/application/insectid/post/1696995116502_kytu.jpg
  return (
    <Box
      sx={{
        backgroundColor: '#fff',
      }}
    >
      <Box
        sx={{
          maxWidth: '1200px',
          margin: '0 auto',
          paddingBottom: '40px',
          display: 'flex',
          flexDirection: { xs: 'column-reverse', md: 'row' },
          justifyContent: { xs: 'center', sx: 'space-between' },
          alignItems: 'center',
          padding: { xs: '20px 0 ', md: '100px 0 96px' },
          position: 'relative',
        }}
      >
        <Box
          sx={{
            position: 'absolute',
            top: { xs: '-200px', sm: '-228px', md: '-250px' },
            right: { xs: '0px', sm: '64px', md: '-130px', lg: '-160px' },
            width: { xs: '500px', md: '630px' },
            height: { xs: '720px', md: '960px' },
            objectFit: 'cover',
            transform: {
              xs: 'rotate(25deg)',
              sm: 'rotate(11deg)',
              md: 'rotate(4deg)',
            },
            '& > img': {
              zIndex: 0,
            },
          }}
        >
          <Image src="/home_back.webp" fill alt="" />
        </Box>
        <Box
          data-aos="fade-right"
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            flexDirection: 'column',
            padding: {
              xs: '50px 20px 0px 20px',
              md: '0 20px',
              lg: '0 60px 0 20px',
            },
            zIndex: 1,
            flex: { md: 1 },
            '& h1': {
              fontWeight: 'bold',
              textAlign: { xs: 'center', md: 'left' },
              fontSize: { xs: '24px', md: '32px', lg: '44px' },
            },
          }}
        >
          <Typography
            variant="h1"
            sx={{
              color: '#cc1a14',
              display: { xs: 'none', md: 'block' },
            }}
          >
            {app?.appName.charAt(0).toUpperCase() + app?.appName.slice(1)}
          </Typography>
          <Typography
            variant="h1"
            sx={{
              color: '#101010',
            }}
          >
            {translate(`app.${app?.link}.description`)}
            {/* AI {app?.link} expert in your pocket */}
          </Typography>
          <Typography
            sx={{
              fontSize: { xs: '14px', lg: '18px' },
              margin: '20px 0 46px 0',
              display: { xs: 'none', md: 'block' },
              color: '#666666',
            }}
          >
            {translate(`app.${app?.link}.content`)}
          </Typography>
          <WrapQRcode
            appName={app?.name}
            linkAndroid={app?.linkAndroid}
            linkIos={app?.linkIos}
            style={{
              margin: '0',
            }}
          />
          <Typography
            sx={{
              fontSize: '14px',
              color: '#000',
              textAlign: 'center',
              marginTop: '10px',
              display: { xs: 'none', md: 'block' },
            }}
          >
            {translate('scan')}
          </Typography>
        </Box>
        <Box
          data-aos="fade-left"
          sx={{
            position: 'relative',
            width: { xs: '400px', sm: '480px', md: '520px' },
            height: { xs: '500px', sm: '540px', md: '640px' },
            zIndex: 2,
            flex: { md: 'inherit' },
            marginRight: { md: '20px' },
            '& > img': {
              objectFit: 'contain',
            },
          }}
        >
          <Image
            src={`/img/${app?.link}/introduce.png`}
            alt="Discover natural world"
            fill
          />
        </Box>
      </Box>
      <Box
        data-aos="flip-up"
        data-aos-duration="1000"
        sx={{
          backgroundColor: '#F8F9FA',
          padding: '20px 20px 20px 0',
          display: 'flex',
          flexDirection: { xs: 'column', md: 'row' },
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Box
          sx={{
            position: 'relative',
            width: { xs: '300px', md: '300px', lg: '340px' },
            height: { xs: '500px', md: '500px', lg: '580px' },
            '& > img': {
              objectFit: 'contain',
            },
            marginBottom: { xs: '40px', md: '0' },
          }}
        >
          <Image src={`/img/${app?.link}/detail.png`} fill alt="" />
        </Box>
        <Box
          sx={{
            width: { xs: '100%', md: '500px', lg: '560px' },
            padding: { xs: '0 40px', md: '0', lg: '0 80px' },
            margin: { md: '0 20px', lg: '0 80px' },
          }}
        >
          <Typography
            variant="h1"
            sx={{
              fontSize: { xs: '32px', lg: '42px' },
              lineHeight: { xs: '40px', lg: '50px' },
              fontWeight: '400',
              color: '#000',
            }}
          >
            {translate(`app.${app?.link}.intro1`)}
            {/* Identify {app?.link} with a snap */}
          </Typography>
          <Typography
            sx={{
              color: '#333',
              fontSize: { xs: '18px', lg: '20px' },
              margin: { xs: '16px 0', lg: '40px 0' },
            }}
          >
            {translate(`app.${app?.link}.detail1`)}
          </Typography>
          <Button
            variant="contained"
            sx={{
              color: '#cc1a14',
              backgroundColor: '#F8F9FA',
              boxShadow: 'none',
              border: '1px solid #cc1a14',
              borderRadius: '50px',
              padding: '10px 40px',
              fontSize: '18px',
              textTransform: 'none',
              '&:hover': {
                backgroundColor: '#cc1a14',
                color: '#fff',
              },
            }}
            onClick={() => setOpen(true)}
          >
            {translate('need')}
          </Button>
        </Box>
      </Box>
      <Box
        data-aos="flip-down"
        data-aos-duration="1000"
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: { xs: 'column-reverse', md: 'row' },
          alignItems: 'center',
          margin: '0 auto',
          width: { xs: '100%', lg: '1200px' },
          padding: '40px 0',
        }}
      >
        <Box
          sx={{
            width: { xs: '100%', md: '560px' },
            padding: { xs: '0 20px 0 40px ', md: '20px', lg: '0 80px' },
          }}
        >
          <Typography
            variant="h1"
            sx={{
              fontSize: { xs: '32px', md: '36px', lg: '42px' },
              lineHeight: '50px',
              fontWeight: '400',
              color: '#000',
            }}
          >
            {translate('intro2')}
          </Typography>
          <Typography
            sx={{
              color: { xs: '#666', md: '#333' },
              fontSize: { xs: '18px', lg: '20px' },
              margin: { xs: '20px 0', lg: '40px 0' },
            }}
          >
            {translate(`app.${app?.link}.detail2`)}
          </Typography>
          <Button
            variant="contained"
            sx={{
              color: '#cc1a14',
              backgroundColor: '#fff',
              boxShadow: 'none',
              border: '1px solid #cc1a14',
              borderRadius: '50px',
              padding: '10px 40px',
              fontSize: '18px',
              textTransform: 'none',
              '&:hover': {
                backgroundColor: '#cc1a14',
                color: '#fff',
              },
            }}
            onClick={() => setOpen(true)}
          >
            {translate('search')}
          </Button>
        </Box>
        <Box
          sx={{
            position: 'relative',
            width: { xs: '300px', md: '320px' },
            height: { xs: '500px', md: '600px' },
            marginBottom: { xs: '40px', md: '0' },
            '& > img': {
              objectFit: 'contain',
            },
          }}
        >
          <Image src={`/img/${app?.link}/database.png`} fill alt="" />
        </Box>
      </Box>
      <ModalDownload
        open={open}
        handleClose={handleClose}
        itemForDownload={app}
      />
    </Box>
  );
};

export default Application;

export async function getStaticPaths() {
  return {
    paths: [
      // String variant:
      '/application/first-post',
      // Object variant:
      { params: { app: 'plantid' } },
    ],
    fallback: true,
  };
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'app'])),
      // Will be passed to the page component as props
    },
  };
}
