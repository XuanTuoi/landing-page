import { getAppName } from '@/src/ultils';
import { Box } from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { useEffect } from 'react';

const NameComponent = () => {
  const router = useRouter();
  const appName = getAppName(router.asPath.split('/')[2]);
  const imageName = router.asPath.split('/')[4];

  const [imgSrc, setImgSrc] = React.useState('');

  useEffect(() => {
    const url = `https://snapapps.sfo3.digitaloceanspaces.com/${appName}/post/${imageName}`;
    setImgSrc(url);
  }, []);
  // https://snapapps.sfo3.digitaloceanspaces.com/fish/post/1697171920086_lamp.png

  return (
    <Box
      sx={{
        padding: '120px 12px',
        position: 'relative',
        width: '100%',
        height: { xs: '50vh', md: '80vh' },
        margin: '0 auto',
        '& > img': {
          objectFit: 'contain',
        },
      }}
    >
      <Image src={imgSrc || ''} fill alt="image" />
    </Box>
  );
};

export default NameComponent;

export async function getStaticPaths() {
  return {
    paths: [
      // String variant:
      '/application/insectid/post/1696995116502_kytu.jpg',
      // Object variant:
      {
        params: {
          app: 'insectid',
          name: '1696995116502_kytu.jpg',
        },
      },
    ],
    fallback: true,
  };
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common'])),
      // Will be passed to the page component as props
    },
  };
}
