import { Box, Typography } from '@mui/material';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React from 'react';
import { useTranslation } from 'next-i18next';

const Term = () => {
  const { t: translate } = useTranslation('term');
  return (
    <Box
      sx={{
        backgroundColor: '#ffffff',
        color: '#4a4a4a',
      }}
    >
      {/* Content */}
      <Box
        sx={{
          background: '#f8f9fc',
          padding: { xs: '20px' },
        }}
      >
        <Box
          sx={{
            maxWidth: '1200px',
            margin: '0 auto',
            fontSize: '1px',
            '& p': {
              lineHeight: '32px',
            },
            '& h2': {
              fontSize: '18px',
              fontWeight: '600',
              lineHeight: '32px',
              color: '#363636',
            },
          }}
        >
          <Typography
            variant="h4"
            sx={{
              textAlign: 'center',
              color: '#363636',
              fontSize: '18px',
              fontWeight: '600',
              display: 'inline',
            }}
          >
            {translate('heading')}
            <Typography
              sx={{
                display: 'inline',
              }}
            >
              &nbsp;{translate('content')}
            </Typography>
          </Typography>

          <Typography>&nbsp; {translate('nd1')}</Typography>
          <Typography>&nbsp; {translate('nd2')}</Typography>
          <Typography variant="h2">{translate('title1')}</Typography>
          <Typography>
            &nbsp;
            {translate('nd3')}
          </Typography>
          <Typography>&nbsp; {translate('nd4')}</Typography>
          <Typography>&nbsp; {translate('nd5')}</Typography>
          <Typography variant="h2">{translate('title2')}</Typography>
          <Typography>&nbsp; {translate('nd6')}</Typography>
        </Box>
      </Box>

      <Typography
        variant="h5"
        sx={{
          textAlign: 'center',
          padding: '32px 0',
          fontSize: '16px',
        }}
      >
        Copyright © Next Vision Limited. All Rights Reserved.
      </Typography>
    </Box>
  );
};

export default Term;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'term'])),
      // Will be passed to the page component as props
    },
  };
}
