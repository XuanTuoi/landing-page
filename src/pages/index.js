import React from 'react';
import { Box, Typography } from '@mui/material';
import { Open_Sans } from 'next/font/google';

import { listApp } from '../contants';
import WrapImg from '../components/WrapImg';

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

const Open_SansFont = Open_Sans({
  subsets: ['latin'],
  weight: ['300', '400', '500', '600', '700', '800'],
});

export default function Home() {
  const { t: translate } = useTranslation('home');

  return (
    <Box
      sx={{
        minHeight: '100vh',
        width: '100%',
        paddingTop: { xs: '40px', md: '40px' },
        paddingBottom: '94px',
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        '& > img': {
          backgroundColor: '#fff',
        },
      }}
    >
      <Box
        sx={{
          backgroundImage: 'url(/bg.jpg)',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat',
          backgroundAttachment: 'fixed',
          position: 'absolute',
          top: '0',
          left: '0',
          width: '100%',
          height: '100%',
          zIndex: '-1',
        }}
      />

      <Box
        sx={{
          color: '#fff',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}
          data-aos="fade-down"
        >
          <Typography
            variant="h2"
            className={Open_SansFont.className}
            sx={{
              fontWeight: '700',
              textAlign: 'center',
              fontSize: { xs: '20px', sm: '52px' },
              lineHeight: { xs: '30px', sm: '60px' },
              padding: { xs: '0 48px', sm: '0' },
              margin: '0 0 24px 0',
              color: '#fff',
            }}
          >
            {translate('title')}
          </Typography>
          <Typography
            sx={{
              padding: { xs: '0px 20px 10px 20px', sx: '10px 0 40px 0' },
              fontWeight: { xs: '500', sm: '400' },
              fontSize: { xs: '14px', sm: '20px' },
              textAlign: 'center',
              width: { xs: '300px', sm: 'auto' },
              color: '#fff',
            }}
            className={Open_SansFont.className}
          >
            {translate('description')}
          </Typography>
        </Box>

        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: {
              xs: '1fr',
              sm: '1fr 1fr',
              md: '1fr 1fr 1fr',
            },
            gridGap: '30px',
            pt: '32px',
          }}
          data-aos="fade-up"
        >
          {listApp.map((item, index) => (
            <Box
              key={index}
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: { xs: '30px', sx: '50px' },
                flexDirection: 'column',
              }}
            >
              <Typography
                sx={{
                  fontWeight: 'bold',
                  fontSize: '18px',
                }}
              >
                {item.name}
              </Typography>
              <WrapImg
                link={item.link || ''}
                logoPath={item.logoPath}
                linkAndroid={item.linkAndroid}
                linkIos={item.linkIos}
              />
            </Box>
          ))}
        </Box>
      </Box>
    </Box>
  );
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['home', 'common'])),
      // Will be passed to the page component as props
    },
  };
}
