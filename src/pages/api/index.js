import useSWR from 'swr';
import axios from 'axios';
const fetcher = async (url) => await axios.get(url).then((res) => res.data);
function DetailFetchAnimalApi(name, searchKey) {
  const { data } = useSWR(
    `https://${name}.snapapps.online/api/v1/${name}?searchKey=${searchKey}`,
    fetcher,
    {
      revalidateIfStale: false,
      revalidateOnFocus: false,
      revalidateOnReconnect: false,
      keepPreviousData: true,
    }
  );
  return data;
}
function PageFetchAnimalApi(name, searchKey, page) {
  const url = `https://${name}.snapapps.online/api/v1/${name}?limit=100&page=${page}${
    searchKey ? `&searchKey=${searchKey}` : ''
  }`;
  const { data, isLoading } = useSWR(url, fetcher, {
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
    keepPreviousData: true,
  });
  return { data, isLoading };
}

export { DetailFetchAnimalApi, PageFetchAnimalApi };
