/** @type {import('next-i18next').UserConfig}
 * Tây Ban Nha: es
 * Tiếng Việt: vn
 * Tiếng Anh: en
 * Tiếng Nhật: ja
 * Tiếng Hàn: ko
 * Tiếng Bồ Đào Nha: pt
 * Tiếng Ả Rập: ar
 */

const path = require('path');
module.exports = {
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'vn', 'ja', 'ko', 'pt', 'es', 'ar'],
  },
  localePath: path.resolve('./public/locales'),
  // react: { useSuspense: false }, //this line
};
