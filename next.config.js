/** @type {import('next').NextConfig} */
const { i18n } = require("./next-i18next.config");
const nextConfig = {
  i18n,
  reactStrictMode: true,
  eslint: {
    ignoreDuringBuilds: true,
  },
  images: {
    domains: [
      "picturemushroom.com",
      "i.ytimg.com",
      "snapapps.sfo3.cdn.digitaloceanspaces.com",
      "snapapps.sfo3.digitaloceanspaces.com",
    ],
    unoptimized: true,
  },
  experimental: {
    scrollRestoration: true,
  },
};

module.exports = nextConfig;
