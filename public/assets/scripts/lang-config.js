window.__GOOGLE_TRANSLATION_CONFIG__ = {
  languages: [
    { title: "English", name: "en" },
    { title: "VietNam", name: "vi" },
    { title: "Spanish", name: "es" },
    { title: "Korean", name: "ko" },
    { title: "Arabic", name: "ar" },
    { title: "Japan", name: "ja" },
    { title: "Portugu", name: "pt" },
  ],
  defaultLanguage: "en",
};
